package com.tsystems.javaschool.tasks.calculator;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.LinkedList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

 /*It's first my implementation of the class.
 I've implemented the class by another way. In another implementation of the class I've
 used 'reverse Polish notation' algorithm.
 You can see another implementation in 'reverse' branch.
 */
public class Calculator {

    /**
     * Regular expression of number.
     */
    private final String number = "-?\\d+\\.?\\d*";

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        // TODO: Implement the logic here
        if (!isValid(statement))
            return null;

        return evaluateInParenthesis(statement);
    }

    /**
     * Method checks the statement on incorrect notation.
     * @param statement is mathematical statement.
     * @return false if statement contains letters, incorrect amount of parenthesis, sign ','
     * and sequence of mathematical signs like '12++34', else return true.
     */
    private boolean isValid(String statement){
        if (statement == null || statement.isEmpty())
            return false;

        if (statement.contains("/0") || statement.matches(".*[^\\.0123456789\\+\\-\\*/\\(\\)].*"))
            return false;

        if (statement.matches(".*[\\.\\+\\-\\*/]{2,}.*"))
            return false;

        return isRightAmountOfParenthesis(statement);
    }

    /**
     * Method counts valid number of parenthesis.
     * @param statement is mathematical statement.
     * @return false if there are positions of parenthesis are incorrect in the statement,
     * else return true.
     */
    private boolean isRightAmountOfParenthesis(String statement){
        LinkedList<Character> stack = new LinkedList<>();
        for (Character symbol : statement.toCharArray()){
            if (symbol != '(' & symbol != ')')
                continue;
            if (symbol == '(') {
                stack.addFirst(symbol);
                continue;
            }
            if (!stack.isEmpty())
                stack.removeFirst();
            else return false;
        }
        return stack.isEmpty();
    }

    /**
     * Method calculates statement in parenthesis.
     * @param statement is parenthesis.
     * @return result of statement in parenthesis.
     */
    private String evaluateInParenthesis(String statement) {
        statement = prepareBeforeEvaluate(statement);
        Pattern pattern = Pattern.compile("\\([^\\(\\)]+\\)");
        Matcher matcher = pattern.matcher(statement);

        while (matcher.find()){
            if (statement.contains("/0"))
                return null;

            String statementInParenthesis = matcher.group().replaceAll("[\\(\\)]", "");

            statementInParenthesis = evaluateStatementInParenthesis(statementInParenthesis,"[\\*/]");
            statementInParenthesis = evaluateStatementInParenthesis(statementInParenthesis,"[\\+\\-]");

            StringBuffer buffer = new StringBuffer();
            matcher.appendReplacement(buffer, statementInParenthesis);
            matcher.appendTail(buffer);

            statement = buffer.toString();
            matcher.reset(statement);
        }
        return statement;
    }

    private String prepareBeforeEvaluate(String statement){
        if (!statement.matches("^\\(.+\\)$"))
            statement = "(" + statement + ")";
        return statement.replaceAll(" ", "");
    }

    /**
     * Method selects simple mathematical statement from input statement.
     * For instance '10 + 12'.
     * @param statement is mathematical statement without parenthesis.
     * @param operation is mathematical sign.
     * @return result of mathematical operation of two numbers.
     */
    private String evaluateStatementInParenthesis(String statement, String operation) {
        Pattern pattern = Pattern.compile(number + operation + number);
        Matcher matcher = pattern.matcher(statement);

        String result = statement;

        while (matcher.find()){
            StringBuffer buffer = new StringBuffer();
            matcher.appendReplacement(buffer, evaluateSimpleStatement(matcher.group()));
            matcher.appendTail(buffer);

            result = buffer.toString();
            matcher.reset(result);
        }
        return result;
    }

    /**
     * Method converts simple mathematical statement from type String to type String[].
     * Where first element of array is first number, second element of array is mathematical sign,
     * third element of array is second number.
     * @param statement is simple mathematical statement.
     * @return result of mathematical operation of two numbers.
     */
    private String evaluateSimpleStatement(String statement) {
        Pattern pattern = Pattern.compile(number);
        Matcher matcher = pattern.matcher(statement);

        matcher.find();
        String firstElement = matcher.group();
        String sign = statement.substring(matcher.end(), matcher.end() + 1);
        String secondElement = statement.substring(matcher.end() + 1);

        String[] elementsOfExpression = {firstElement, sign, secondElement};
        return calculate(elementsOfExpression);
    }

    /**
     * Method calculates two numbers.
     * @param elementsOfStatement array which contains two numbers and a mathematical sign ('+', '-', '*', '/').
     * @return result of mathematical operation of two numbers.
     */
    private String calculate(String[] elementsOfStatement) {
        BigDecimal result = new BigDecimal(elementsOfStatement[0]);
        BigDecimal secondNumber = new BigDecimal(elementsOfStatement[2]);
        char sign = elementsOfStatement[1].charAt(0);

        switch (sign){
            case '+': return result.add(secondNumber, MathContext.DECIMAL32).toString();
            case '-': return result.subtract(secondNumber, MathContext.DECIMAL32).toString();
            case '*': return result.multiply(secondNumber, MathContext.DECIMAL32).stripTrailingZeros().toPlainString();
            default: return result.divide(secondNumber, MathContext.DECIMAL32).stripTrailingZeros().toPlainString();
        }
    }
}
