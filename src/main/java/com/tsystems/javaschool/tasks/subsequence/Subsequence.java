package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;
import java.util.ListIterator;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */

    public boolean find(List x, List y) {
        // TODO: Implement the logic here
        checkToNull(x, y);

        if (x.isEmpty())
            return true;

        if (x.size() > y.size())
            return false;

        ListIterator xIterator = x.listIterator();
        ListIterator yIterator = y.listIterator();

        Object currentElementInX = xIterator.next();

        while (yIterator.hasNext()){
            if (currentElementInX.equals(yIterator.next())){
                xIterator.remove();
                if (xIterator.hasNext())
                    currentElementInX = xIterator.next();
            }
            yIterator.remove();
        }
        return x.isEmpty();
    }

    /**
     * Method checks first and second sequences on null.
     *
     * @param x first sequence.
     * @param y second sequence.
     * @throws IllegalArgumentException if either of sequences is null.
     */
    private void checkToNull(List x, List y){
        if (x == null || y == null)
            throw new IllegalArgumentException();
    }
}
