package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Previous minimum element from input list.
     */
    private Integer previousMin = Integer.MIN_VALUE;
    /**
     * Number of columns in the pyramid.
     */
    private int numberOfColumns;
    /**
     * Number of rows in the pyramid.
     */
    private int numberOfRows = 1;

    private int[][] pyramid;

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        // TODO : Implement your solution here
        isValid(inputNumbers);

        numberOfColumns = 2*numberOfRows - 1;
        pyramid = new int[numberOfRows][numberOfColumns];

        //Building of the pyramid
        for (int i = 0; i < numberOfRows; i++)
            for (int j = 0; j <= i; j++) {
                int min = getNextMin(inputNumbers);
                setNextMin(i, j, min);
            }
        return pyramid;
    }

    /**
     * Method checks an opportunity of pyramid's building.
     * @param inputNumbers list of numbers.
     * @throws CannotBuildPyramidException if the pyramid cannot be built.
     */
    private void isValid(List<Integer> inputNumbers){
        if (inputNumbers.contains(null))
            throw new CannotBuildPyramidException();

        if (Collections.max(inputNumbers).equals(Collections.min(inputNumbers)))
            throw new CannotBuildPyramidException();

        int realSize = inputNumbers.size();
        int currentSize = numberOfRows;

        while (currentSize < realSize){
            currentSize += ++numberOfRows;

            if (currentSize > realSize)
                throw new CannotBuildPyramidException();
        }
    }

    /**
     * Method returns next minimum element from input list.
     * @param inputNumbers list of numbers.
     * @return minimum element.
     */
    private int getNextMin(List<Integer> inputNumbers){
        int min = Integer.MAX_VALUE;

        for (int i = 0; i < inputNumbers.size(); i++)
            if (inputNumbers.get(i).compareTo(min) <= 0 & inputNumbers.get(i).compareTo(previousMin) > 0) {
                min = inputNumbers.get(i);
            }

        previousMin = min;
        return min;
    }

    /**
     * Method sets element in the pyramid.
     * @param numberOfRow number of current row in the pyramid.
     * @param numberOfElement number of element in the pyramid's row.
     * @param min minimum element.
     */
    private void setNextMin(int numberOfRow, int numberOfElement, int min){
        int firstPosition = numberOfColumns/2 - numberOfRow;
        int offset = 2*numberOfElement;
        pyramid[numberOfRow][firstPosition + offset] = min;
    }
}
